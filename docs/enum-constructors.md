[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
===

# **Enum Constructors**
## **Parameters**

Enum._**[EnumName](#enumname:)**_([[`EnumParameters`](#enumparameters:)], **[`EnumValues`](#enumvalues:)**)


### **EnumName:**
    Name of your enum

### **EnumParameters:**
#### Base: Number (2-36)
##### default: 10

    The EnumValue base number.
    This parameter will be use to display EnumValue values, in a more readable way.

```
Enum.YourEnumName(/*BASE:*/ 16, /*Mandatory: EnumValues*/)
```

#### Class: EnumValue subclass
##### default: EnumValue

    This allow to override EnumValue behavior or add new properties and methods on your EnumValues.

```
Enum.YourEnumName(/*CLASS:*/ EnumValueSubclass, /*Mandatory: EnumValues*/)
```

#### All: Object
```
{
    base: Number (2-36)
    class: EnumValue subclass
}
```
See above:
- **[Base](#base:-number-(2-36))**
- **[Class](#class:-enumvalue-subclass)**

```
Enum.YourEnumName(/* ALL OBJECT: */{
        base: 2,
        class: EnumValueSubclass
    },
    /* Mandatory: EnumValues */
)
```
### **EnumValues:**
#### Values names: Strings parameters list
```
    'NAME1', 'NAME2', ...
```
**NAMEx**: valid identifier _(otherwise you won't be able to access it with '.' notation)_

```
Enum.YourEnumName(/* Optional: EnumParameters, */
    'VALUE1',
    'VALUE2',
    ...
)
```

#### NameValue Object litteral
```
    {
        NAME: value,
        ...
    }
```
- **NAME**: valid identifier _(otherwise you won't be able to access it with '.' notation)_
- **value**: Number|String Number (or any primitive types, but not recommended),

```
Enum.YourEnumName(/* Optional: EnumParameters, */ {
    VALUE1: 2,
    VALUE2: {}, // value: 3
    VALUE3: 8,
    VALUE4: {}, // value: 9
    VALUE5: {}, // value: 10
    /* ... */
})
```

#### EnumValue config Objet
    {
        NAME: {
            [value: Number|String (or anything, even object, but not recommended)],
            class: EnumValue subclass
        },
        ...
    }
- **NAME**: valid identifier _(otherwise you won't be able to access it with '.' notation)_
- **value**: Number|String Number (or anything, even object, but not recommended because some methods won't work),
- **class**: a subclass of the class passed as EnumParameter, if none passed, an EnumValue subclass.
(See above: **[Class](#class:-enumvalue-subclass)**)

Related to: [Custom Methods](custom-methods.md)

```
Enum.YourEnumName(/* Optional: EnumParameters, */ {
    VALUE1: {
        value: 2,
        class: CustomEnumValue1
    },
    VALUE2: {}, // value: 3, class: EnumValue
    VALUE3: {
        value: 8,
        class: CustomEnumValue
    },
    VALUE4: {}, // value: 9, class: EnumValue
    VALUE5: {}, // value: 10, class: EnumValue
    /* ... */
})
```

---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
