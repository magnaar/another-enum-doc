[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
===

## *`In progress`*

# Special Cases
## Not Numeric Values
### Strings Values
```
const Values = Enum.Values({
    VALUE1: 'Value1',
    VALUE2: 'Value2'
}
// or
const Values = Enum.Values({
    VALUE1: { value: 'Value1' },
    VALUE2: { value: 'Value2' }
}

+Values.VALUE1 // => NaN
Values.VALUE1.value // => 'Value1'
```

## Function Values
```
const Values = Enum.Values({
    VALUE1: { value: () => {} },
})

+Values.VALUE1 // => NaN
Values.VALUE1.value // => function () => {}

// This notation can't work
const Values = Enum.Values({
    VALUE1: () => 'Hello',
    VALUE2: () => 'World'
}
// Because it's already used by the "recursive" constructor
```

## Object Values
```
const Values = Enum.Values({
    VALUE1: { value: {} },
})

+Values.VALUE1 // => NaN
Values.VALUE1.value // => object {}

// This notation can't work
const Values = Enum.Values({
    VALUE1: { foo: 'Hello' },
    VALUE2: { bar: 'World' }
}
// Because it's already used by the all object literal constructor
```

## Value ignored
```
class CustomEnumValue extends EnumValue
{
    init() { this._value = 4 }
    get value() { return this._value }
}

const Values = Enum.Values({
    VALUE1: {
        class: CustomEnumValue,
        value: 1 // This will be overwritten by the init method
    },
})

Values.VALUE1 // => 4
```
---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
