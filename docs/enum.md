[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
===

# **Enum** _(`returned by Enum.YourName(...)`)_
## **get**(_`value`_)
> **Return the EnumValue for the given value**
>
> ```
> // With this instanciation, value and index are the same
> const Colors = Enum.Colors('RED', 'GREEN', 'BLUE')
> Colors.get(1) // => Colors.GREEN
> 
> // With this instanciation, value and index are different
> const HexaColors = Enum.HexaColors({
>     RED: 0xFF0000,
>     GREEN: 0x00FF00,
>     BLUE: 0x0000FF
> })
> HexaColors.get(0x00FF00) // => HexaColors.GREEN
> ```


## **getAt**(_`index`_)
> **Return value for the given index (declaration order)**
> 
> ```
> // With this instanciation, value and index are the same
> const Colors = Enum.Colors('RED', 'GREEN', 'BLUE')
> Colors.getAt(1) // => Colors.GREEN
> 
> // With this instanciation, value and index are different
> const HexaColors = Enum.HexaColors({
>     RED: 0xFF0000,
>     GREEN: 0x00FF00,
>     BLUE: 0x0000FF
> })
> HexaColors.getAt(1) // => Colors.GREEN
> ```


## **hasIn**(_`mask`, ...`EnumValues`|`names`|`values`_)
> **Indicates if the `mask` contains `ALL` the given `values`**
> 
> ```
> const HexaColors = Enum.HexaColors({
>     RED: 0xFF0000,
>     GREEN: 0x00FF00,
>     BLUE: 0x0000FF
> })
> 
> const mask = 0xFFFF00
> HexaColors.hasIn(mask, Colors.RED, Colors.GREEN) // => true
> HexaColors.hasIn(mask, Colors.RED, Colors.BLUE) // => false, mask doesn't contain BLUE
> 
> HexaColors.hasIn(mask, 'RED', 'GREEN') // => true
> HexaColors.hasIn(mask, 0xFF0000, 0x00FF00) // => true
> 
> // You can even mix them
> HexaColors.hasIn(mask, 0x00FF00, 'RED') // => true
> HexaColors.hasIn(mask, Colors.GREEN, 'RED') // => true
> ```


## **in**(_`value`_)
> **Returns an `array` of all the `EnumValues contained` in the given `mask`**
> 
> ```
> const HexaColors = Enum.HexaColors({
>     RED: 0xFF0000,
>     GREEN: 0x00FF00,
>     BLUE: 0x0000FF
> })
> const mask = 0xFFFF00
> HexaColors.in(mask) // => [HexaColors.RED, HexaColors.GREEN]
> ```


## **length `{get}`**
> **Returns the `number` of `EnumValues` in the enum**
> 
> ```
> const Colors = Enum.Colors('RED', 'GREEN', 'BLUE')
> Colors.length // => 3
> ```


## **name `{get}`**
> **Return the `Enum's name`**
> 
> ```
> let anotherEnum = Enum.Colors('RED', 'GREEN', 'BLUE')
> anotherEnum.name // => 'Colors'
> 
> anotherEnum = Enum.HexaColors({
>     RED: 0xFF0000,
>     GREEN: 0x00FF00,
>     BLUE: 0x0000FF
> })
> anotherEnum.name // => 'HexaColors'
> ```


## **parse**(_`object`_)
> **Return the given `object` with its `string properties`** (that match EnumValue long name)
> 
> **`replaced` by EnumValues**
> 
> /!\ It also modifies the given object /!\
> 
> ```
> const obj = { 
>     whateverProp: 'Hello World',
>     colorProp: 'Colors.BLUE',
>     deepProp: {
>         hexaColorProp: 'HexaColors.GREEN'
>     }
> }
> Colors.parse(obj) // obj.colorProp => EnumValue Colors.BLUE
> HexaColors.parse(obj) // obj.deepProp.hexaColorProp => EnumValue HeaxeColors.GREEN
> 
> // obj => {
> //    whateverProp: 'Hello World',
> //    colorProp: EnumValue Colors.BLUE,
> //    deepProp: {
> //        hexaColorProp: EnumValue HexaColors.GREEN
> //    }
> // }
> ```


## **parse**(_`string`_)
> **Return an `EnumValue` for the given name**
> 
> ```
> Colors.parse('Colors.RED') // EnumValue RED
> Colors.parse('HexaColors.RED') // null
> Colors.parse('Not an enum value') // null
> ```


## **toString**(_`number`|`EnumValue`_)
> 
> ```
> HexaColors.toString() // Colors(RED|GREEN|BLUE)
> HexaColors.toString(0x00FF00) // Colors.GREEN(16:00FF00)
> HexaColors.toString(0xFF00FF) // Colors.[RED|BLUE](16:FF00FF)
> ```

## **`Symbol.iterator`**

---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)


