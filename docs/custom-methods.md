[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
===

# **Custom methods**
Related to: [Enum Constructors](enum-constructors.md)

## **For EnumValues**
By specifying a custom class as EnumValue constuctor,
you will be able to access to all your methods and properties
on your EnumValues (and also on the Enum object)

```
class CustomEnumValue extends EnumValue
{
    init()
    {
        this._square = this.value * this.value
        this._regex = new RegExp(`\{{{this.name}\}}`, 'gi')
    }

    replace(str)
    {
        return str.replace(this._regex, this.value)
    }

    // It's better to set read-only properties, 
    // than read/write properties or fields
    // Because it assures you that no one will change your enum values
    // Enum values should be immutable
    get square()
    {
        return this._square
    }
}

const Example = Enum.Example(CustomEnumValue, {
    VALUE_1: 2,
    VALUE_2: 4,
})

Example.VALUE_1.replace('the VALUE_1 value is: {VALUE_1}')
// => 'the VALUE_1 value is: 2'
Example.VALUE_1.square // => 4

Example.VALUE_2.method('the VALUE_2 value is: {VALUE_2}')
// => 'the VALUE_2 value is: 16'
Example.VALUE_2.square // => 16
```

## **Static methods/properties**
Static members aren't accessible on the EnumValues objects,
but, you can call them from the Enum object.

## **Customize each value**
You can have a set of methods/properties for each value of your enum.
By using the [constructor with the EnumValue config object](enum-constructors.md#enumvalue-config-objet),
you can specify an implementation for each EnumValue.

```
class CustomEnumValue1 extends EnumValue
{
    get hello() { return 'Hello' }
}

class CustomEnumValue2 extends EnumValue
{
    get world() { return 'World' }
}

const Example = Enum.Example({
    'VALUE_1': { class: CustomEnumValue1 },
    'VALUE_2': { class: CustomEnumValue2 }
})

// Example.VALUE_1.hello => 'Hello'
// Example.VALUE_1.world => undefined

// Example.VALUE_2.hello => undefined
// Example.VALUE_2.world => 'World'
```

## **For Enums**

By specifying a custom class as EnumValue constuctor,
you will be able to access to all your methods and properties
on your Enum object (and also on the EnumValues)

```
class CustomEnumValue extends EnumValue
{
    init()
    {
        this._square = this.value * this.value
        this._regex = new RegExp(`\{{{this.name}\}}`, 'gi')
    }

    replace(str)
    {
        return str.replace(this._regex, this.value)
    }

    // It's better to set read-only properties, 
    // than read/write properties or fields
    // Because it assures you that no one will change your enum values
    // Enum values should be immutable
    get square()
    {
        return this._square
    }
}

const Example = Enum.Example(CustomEnumValue, {
    VALUE_1: 2,
    VALUE_2: 4,
})

Example.replace(Example.VALUE_1, 'the VALUE_1 value is: {VALUE_1}')
// => 'the VALUE_1 value is: 2'
Example.square(Example.VALUE_1) // => 4

Example.VALUE_2.method('the VALUE_2 value is: {VALUE_2}')
// => 'the VALUE_2 value is: 16'
Example.VALUE_2.square // => 16
```
In these examples, we passed a EnumValue object as this parameter,
but you can also pass the EnumValue name (String) or its value (Number) instead.

## **Static methods/properties**
Static members of your custom EnumValue class, can be called directly from the Enum object.
However these members aren't accessible on the EnumValue objects.
To summerize, static members are Enum object specific, not static are for both.

```
class TimeUnitEnumValue extends EnumValue
{
    init(parent)
    {
        if (! TimeUnitEnumValue.reversedUnits)
            TimeUnitEnumValue.reversedUnits
                = Object.values(parent).reverse()
    }

    static splitMsAmount(msAmount)
    {
        const amounts = {}
        for (const unit of TimeUnitEnumValue.reversedUnits)
        {
            amounts[unit] = (msAmount / unit) | 0
            msAmount = msAmount % unit
        }
        return amounts
    }

    static getMsAmountFromUnit(text)
    {
        text = text.name || text.toUpperCase()
        for (const unit in TimeUnit)
            if (text == unit || text == unit + 'S')
                return +TimeUnit[unit]
        return null
    }
}

const TimeUnit = Enum
    .TimeUnit(TimeUnitEnumValue, {
        'MINUTE': 1000 * 60,
        'HOUR': 1000 * 3600,
        'DAY': 1000 * 3600 * 24,
        'WEEK': 1000 * 3600 * 24 * 7,
        'MONTH': 1000 * 3600 * 24 * 30,
        'YEAR': 1000 * 3600 * 24 * 30 * 365
    })

TimeUnit.getMsAmountFromUnit("WEEKS") // => 604800000
TimeUnit.splitMsAmount(123456000)
// => {
    YEAR: 0,
    MONTH: 0,
    WEEK: 0,
    DAY: 1,
    HOUR: 10,
    MINUTE: 17
}
```

## **Customize each value**
You can't customize each values by the Enum constructor,
you have to specify the customisation for each value
by using the [constructor with the EnumValue config object](enum-constructors.md#enumvalue-config-objet),

## **How to create a custom EnumValue class**

You can override all the methods and properties in the [EnumValue methods listing](enum-value.md), but not the constructor.

If you have to initialize some stuff, use the `init(parent : Enum object)` instead.


---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
