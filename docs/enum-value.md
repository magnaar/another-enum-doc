[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
===

# **EnumValue**
## **index `{get}`**
> **Return the `position` of the value in the enum object list**
> 
> ```
> Colors.RED.index // 0
> HexaColors.RED.index // 0
> CssColors.RED.index // 0
> ```

## **init(_`enumParent`_)**
> **Initialize the EnumValue**
> 
> It's `called outside` the EnumValue `constructor`
> 
> ```
> Colors.RED.index // 0
> HexaColors.RED.index // 0
> CssColors.RED.index // 0
> ```

## **isIn**(_`mask`_)
> **Indicate if the `EnumValue` is contained in the given `mask`**
> 
> ```
> const value = 0xFFFF00
> HexaColors.RED.isIn(value) // true
> HexaColors.GREEN.isIn(value) // true
> HexaColors.BLUE.isIn(value) // false
> ```

## **longName `{get}`**
> **Return the `Enum` and the `EnumValue's name`**
> 
> ```
> Colors.RED.longName // 'Colors.RED'
> HexaColors.RED.longName // 'HexaColors.RED'
> CssColors.RED.longName // 'CssColors.RED'
> ```

## **name `{get}`**
> **Return the `EnumValue's name`**
> 
> ```
> Colors.RED.name // 'RED'
> HexaColors.RED.name // 'RED'
> CssColors.RED.name // 'RED'
> ```

## **stringValue `{get}`**
> **Return as `string` the `EnumValue's value` converted in the `Enum's base`**
> 
> ```
> Colors.RED.stringValue // '0'
> HexaColors.RED.stringValue // 'FF0000'
> CssColors.RED.stringValue // '#FF0000'
> BinColors.RED.stringValue // '100'
> ```

## **toString()**

## **value `{get}`**
> **Return the `EnumValue's value` as `base 10`**
> 
> ```
> Colors.RED.value // 0
> HexaColors.RED.value // 16711680
> CssColors.RED.value // '#FF0000'
> BinColors.RED.value // 4
> ```

## **`Symbol.toPrimitive`**
>

---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
