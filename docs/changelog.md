[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
===

# Changelog
> # 1.0.1
> ### Fixes
> - Fix a bug where [stringValue](docs/enum-value.md#stringValueget) property ([EnumValue](docs/enum-value.md)) wasn't of the good length
> - Make [stringValue](docs/enum-value.md#stringValueget) property safer (can't throw exception), in case of an error
occuring in the [EnumValue](docs/enum-value.md) [stringValue](docs/enum-value.md#init) method

---
> # 1.0.0
> ### Majors:
> - Add [custom constructors](docs/enum-constructors.md)
>     - Enable Custom methods/properties for [Enum](docs/enum.md) and [EnumValue](docs/enum-value.md)s
> - Add [specific custom EnumValue constructors](docs/enum-constructors.md#enumvalue-config-objet)
> - [Parse properties](docs/enum.md#parseobject) within an object
>
> ### Minors:
> - Hide the private fields when displaying [Enum](docs/enum.md) and [EnumValue](docs/enum-value.md)s
> - Display the [toString()](docs/enum-value.md#toString) result when using `console.log()` on [EnumValue](docs/enum-value.md)
>
> ### Fixes:
> - Methods "[in](enum.md#invalue)" doesn't show the 0 flag, if the value is not 0


---
> # 0.0.5
> ### Fixes
> - Version change to avoid an invalid version error


---
> # 0.0.4
> ### Minors
> - Move tests into a [submodule](https://gitlab.com/magnaar/another-enum-test)
> - Add .npmignore
> 
> ## 0.0.4-t1
> ### Fixes
> - Strategy change for test import
> 
> ## 0.0.4-t2
> ### Fixes
> - Update package.json
> - Update README


---
> # 0.0.3
> ### Majors
> - De/Serializating feature
> 
> ### Fixes
> - Removal of the check for Enum name
> 
> ## 0.0.3-r1
> ### Fixes
> - Update README
> 
> ## 0.0.3-r2
> ### Fixes
> - Update README


---
> # 0.0.2
> ### Majors
> - Bitmask feature
> 
> ## 0.0.2-r1
> ### Fixes
> - Update README
> - Add known-issues.test.js


---
> # 0.0.1
> ### Majors
> - Initial features


---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)
